#!/usr/bin/env python2
"""cliphist.py: utility to print clipit history file.
If an argument is passed on the command line, it will
be used as a separator, otherwise history items are
separated by a blank line. """

import struct, os, sys

homedir  = os.environ['HOME']
histfile = homedir + '/.local/share/clipit/history'

with open(histfile,'rb') as f:
    f.read(68)
    size,_ = struct.unpack('2i',f.read(8))
    while (size > 0):
        item = f.read(size)
        if not item.strip() == '':
            print item.strip()
        _,_,_,size,_ = struct.unpack('5i',f.read(20))
